//
//PARAMETROS POR DEFECTO
//

function newFunction(name, age, country) {
    name = name ||'oscar';
    age = age || 32;
    country = country || 'MX';
    console.log(name, age, country);
}

//ES6

function newFunction2(name = 'oscar', age = 32, country = 'MX') {
    console.log(name, age, country);
}

newFunction2();
newFunction2('Ricardo', 23, 'CO');


//
//TEMPLATE LITERALS
//

let hello = "hello";
let world = "world";

let epicPhrase = hello + ' ' + world;
console.log(epicPhrase);

//ES6

let epicPhrase2 = `${hello} ${world}`;
console.log(epicPhrase2);


//
//MULTILINEA EN STRINGS
//

let lorem = "Frase a separar \n"
+ "otra frase separada";

console.log(lorem);

//ES6

let lorem2 = `Segunda frase a separar
segunda frase separada
`;

console.log(lorem2);


//
//DESESTRUCTURACION DE ELEMENTOS
//

let person = {
    'name' : 'oscar',
    'age' : '32',
    'country' : 'MX'
}

console.log(person.name, person.age, person.country);

//ES6

let { name, age, country } = person;
console.log(name, age, country);


//
//OPERADOR DE PROPAGACION (SPREAD OPERATOR)
//

let team1 = ['Oscar', 'Julian', 'Ricard'];
let team2 = ['Valeria', 'Jessica', 'Camila'];

let education = ['David', ...team1, ...team2];

console.log(education);


//
//VAR, LET Y CONST
//

{
    var globalVar = 'Global Var';
}
console.log(globalVar);
//Var es de scope global

{
    let globalLet = 'Global Let';
    console.log(globalLet);
}
//Let es de scope local

const a = 'b';
a = 'a';
console.log(a);
//los valores de Const no cambian


//
//CREACION DE OBJETOS
//

let name = 'Oscar';
let age = 32;

obj = { name: name, age: age };
console.log(obj);

//ES6

obj2 = { name, age };
console.log(obj2);


//
//ARROW FUNCTION
//

const names = [
    {name: 'Oscar', age: 32},
    {name: 'Jessica', age: 27}
]

let listOfNames = names.map(function (item) {
    console.log(item.name)
})

//ES6

let listOfNames2 = names.map(item => console.log(item.name));

const listOfNames3 = (name, age, country) => {
    ...
}
//para varios parametros

const listOfNames3 = name => {
    ...
}
//para un solo parametro

const square = num => num * num;
//evita usar return


//
//PROMESAS
//

//Sirve para hacer peticiones a APIs
const helloPromise = () => {
    return new Promise((resolve, reject) => {
        if (true) {
            resolve('Hey!');
        } else {
            reject('Upss!');
        }
    });
}

helloPromise()
  .then(response => console.log(response))
  .then(() => console.log('otro then'))
  .catch(error => console.log(error));


//
//CLASES
//

class calculator {
    constructor() {
        this.valueA = 0;
        this.valueB = 0;
    }
    sum(valueA, valueB) {
        this.valueA = valueA;
        this.valueB = valueB;
        return this.valueA + this.valueB;
    }
}

const calc = new calculator();
console.log(calc.sum(2,2));


//
//MODULOS
//

import { hello } from './module';
hello();


//
//GENERADORES
//

function* helloWorld() {
    if (true) {
        yield 'Hello, ';
    }
    if (true) {
        yield 'world';
    }
}

const generatorHello = helloWorld();
console.log(generatorHello.next().value);
console.log(generatorHello.next().value);
console.log(generatorHello.next().value);
