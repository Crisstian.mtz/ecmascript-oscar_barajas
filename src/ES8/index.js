//
//OBJECT ENTRIES
//

const data = {
    'frontend' : 'Oscar',
    'backend' : 'Isabel',
    'design' : 'Ana',
}

const entries = Object.entries(data);
console.log(entries);
console.log(entries.length);


//
//OBJECT VALUES
//

const data  = {
    'frontend' : 'Oscar',
    'backend' : 'Isabel',
    'design' : 'Ana',
}

const values = Object.values(data)
console.log(values);
console.log(values.length);


//
//PADDING
//

const string = 'Hello!'
console.log(string.padStart(8, 'hi'));
console.log(string.padEnd(8, ':)'));


//
//TRAILING COMA
//

const data  = {
    'frontend' : 'Oscar',
}


//
//ASYNC AWAIT
//

const helloWorld = () => {
    return new Promise((resolve, reject) => {
        (true)
        ? setTimeout(() => resolve('Hello world'), 3000)
        : reject(new Error('Test error'))
    })
};

const helloAsync = async () => {
    const hello = await helloWorld();
    console.log(hello)
};

helloAsync();

const anotherFunction = async () => {
    try {
        const hello = await helloWorld();
        console.log(hello);
    } catch (error) {
        console.log(error);
    }
};

anotherFunction();
